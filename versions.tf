terraform {
  required_providers {
    hcloud = {
      source = "terraform-providers/hcloud"
      version = "1.31.1"
    }
  }
}
