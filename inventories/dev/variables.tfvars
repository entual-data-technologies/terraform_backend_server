# Environment
environment="dev"

# Name variable definition
hostname="emt.dev.entual.de"

# Defining a variable source OS image for an instance
image="ubuntu-20.04"

# Definition of an instance type variable depending on the choice of tariff
server_type="cx11"

# Definition of the region in which the instance will be created
location="nbg1"

# Definition of the variable for enabling backups
enable_backups=false
