#!/bin/bash

terraform workspace select dev
terraform destroy -var-file=inventories/dev/variables.tfvars -var-file=secret-variables.tfvars -auto-approve