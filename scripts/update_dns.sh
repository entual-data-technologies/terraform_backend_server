#!/bin/bash

source ../secret-variables.tfvars

API=https://dns.hetzner.com/api/v1
ZONE_NAME=entual.de
HOST=emt

ENVIRONMENT=$1
IP=$2

rx='([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])'

# check provided environment
if [[ ${ENVIRONMENT} != 'dev' ]]; then
  echo "Environment '${ENVIRONMENT}' is invalid. Must be 'dev'."
  exit 1
fi

# check provided ip address
if ! [[ ${IP} =~ ^$rx\.$rx\.$rx\.$rx$ ]]; then
  echo "Invalid IPv4 address given: ${IP}"
  exit 1
fi

# check API token for DNS API
if [[ -n $3 ]]; then
  dns_api_token=$3
elif [[ -n $DNS_API_TOKEN ]]; then
  dns_api_token=$DNS_API_TOKEN
else
  echo "ERROR: No DNS API token found. Specify it either by setting the environment variable DNS_API_TOKEN or by" \
    "using the third parameter."
  exit 1
fi

#1 Get zone-id from DNS API (In first API call, check authentication additionally.
ZONE_RESPONSE=$(curl -sfS "${API}/zones?name=${ZONE_NAME}" -H "Auth-API-Token: ${dns_api_token}")
if [ $? -ne 0 ]; then
  exit 1
elif echo "${ZONE_RESPONSE}" | jq -e '.message' > /dev/null; then
  echo "ERROR: $(echo "${ZONE_RESPONSE}" | jq -r '.message')" && exit 1
fi
ZONE_ID=$(echo "${ZONE_RESPONSE}" | jq -er ".zones[] | select(.name==\"${ZONE_NAME}\").id")
if [[ -z "${ZONE_ID}" && ${ZONE_ID} != 'null' ]]; then
  echo "ERROR: Could not retrieve the zone id from DNS API:"
  echo "${ZONE_RESPONSE}" | jq
fi

#2 get record
RECORD_RESPONSE=$(curl -sfS "${API}/records?zone_id=${ZONE_ID}" -H "Auth-API-Token: ${dns_api_token}")
if [ $? -ne 0 ]; then
  exit 1
elif echo "${RECORD_RESPONSE}" | jq -e '.message' > /dev/null; then
  echo "ERROR: $(echo "${RECORD_RESPONSE}" | jq -r '.message')" && exit 1
fi
RECORD_ID=$(echo "${RECORD_RESPONSE}" | jq -r ".records[] | select(.name==\"${HOST}${ENVIRONMENT}\").id")
if [[ -z ${RECORD_ID} && ${RECORD_ID} != 'null' ]]; then
  echo "ERROR: Could not retrieve the record id from DNS API:"
  echo "${RECORD_RESPONSE}" | jq
fi


#3 update record
UPDATE_RESPONSE=$(curl -sfS -X "PUT" "${API}/records/${RECORD_ID}" \
  -H "Content-Type: application/json" \
  -H "Auth-API-Token: ${dns_api_token}" \
  -d '{
        "value": "'"${IP}"'",
        "ttl": 60,
        "type": "A",
        "name": "'"${HOST}${ENVIRONMENT}"'",
        "zone_id": "'"${ZONE_ID}"'"
      }')
if [ $? -ne 0 ]; then exit 1
elif echo "${UPDATE_RESPONSE}" | jq -e '.message' > /dev/null; then
  echo "ERROR: $(echo "${UPDATE_RESPONSE}" | jq -r '.message')" && exit 1
elif echo "${UPDATE_RESPONSE}" | jq -e '.error' > /dev/null; then
  echo "ERROR: $(echo "${UPDATE_RESPONSE}" | jq -r '.error')" && exit 1
fi

UPDATED_RECORD=$(echo "${UPDATE_RESULT}" | jq -r ".record.id")
if [[ -n $UPDATED_RECORD && $UPDATED_RECORD != 'null' ]]; then
  echo "ERROR: "
  echo "${UPDATE_RESPONSE}" | jq
  exit 1
else
  echo "DNS record ${HOST}${ENVIRONMENT}.${ZONE_NAME} successfully updated to ${IP}."
fi
