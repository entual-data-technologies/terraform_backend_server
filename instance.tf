# Create new ssh key in the cloud
resource "hcloud_ssh_key" "ssh_key" {
  name = var.hostname
  public_key = file("inventories/${var.environment}/keys/id_rsa.pub")
}

# Create a new cloud server
resource "hcloud_server" "emt" {
  name = var.hostname
  image = var.image
  server_type = var.server_type
  location = var.location
  backups = var.enable_backups
  ssh_keys = [hcloud_ssh_key.ssh_key.id]

  # update DNS server with new IP address
  provisioner "local-exec" {
    command = "DNS_API_TOKEN=${var.dns_api_token} ./scripts/update_dns.sh ${var.environment} ${hcloud_server.emt.ipv4_address}"
  }

  provisioner "local-exec" {
    command = "sleep 60"
  }

  # wait 30 sek to boot the server and run ansible on new server
  provisioner "local-exec" {
    working_dir = "../ansible_playbook_emt"
    command = "ANSIBLE_HOST_KEY_CHECKING=False ./run_playbook.sh -e ${var.environment} -fp"
  }
}
