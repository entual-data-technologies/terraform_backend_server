# Environment
variable "environment" {
  type = string
}

# Token for Hetzner Cloud definition
variable "hcloud_token" {
  type = string
  sensitive = true
}

# Token for DNS server definition
variable "dns_api_token" {
  type = string
  sensitive = true
}

# Name variable definition
variable "hostname" {
  type = string
}

# Defining a variable source OS image for an instance
variable "image" {
  type = string
}

# Definition of an instance type variable depending on the choice of tariff
variable "server_type" {
  type = string
}

# Definition of the region in which the instance will be created
variable "location" {
  type = string
}

# Definition of the variable for enabling backups
variable "enable_backups" {
  type = bool
}
