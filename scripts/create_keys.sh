#!/usr/bin/env bash

mkdir -p ../inventories/$1/keys
ssh-keygen -t rsa -C "thomas.paulus@entual.de" -f ../inventories/$1/keys/id_rsa